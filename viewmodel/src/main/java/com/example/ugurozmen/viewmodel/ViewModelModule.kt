package com.example.ugurozmen.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Named
import javax.inject.Provider

@Module
class ViewModelModule {
    @Named("MainDispatcher")
    @Provides
    fun mainDispatcher(): CoroutineDispatcher = Dispatchers.Main

    @Provides
    fun provideViewModelProviderFactory(
        mostViewedCategoryVMProvider: Provider<MostViewedCategoryViewModel>,
        categoriesVMProvider: Provider<CategoriesViewModel>
    ): ViewModelProvider.Factory =
        object : ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel?> create(modelClass: Class<T>): T =
                when (modelClass) {
                    MostViewedCategoryViewModel::class.java -> mostViewedCategoryVMProvider.get() as T
                    CategoriesViewModel::class.java -> categoriesVMProvider.get() as T
                    else -> throw IllegalArgumentException("Unexpected modelClass: $modelClass")
                }
        }
}