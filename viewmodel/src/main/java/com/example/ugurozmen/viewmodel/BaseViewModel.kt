package com.example.ugurozmen.viewmodel

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*

sealed class NavigationTarget {
    object None : NavigationTarget()
    data class Categories(val addToBackStack: Boolean = true) : NavigationTarget()
    data class Images(val categoryName: String) : NavigationTarget()
}

sealed class Content<out T> {
    object Loading : Content<Nothing>()
    data class Error(val description: String) : Content<Nothing>()
    data class Loaded<T>(val content: T) : Content<T>()
}

abstract class BaseViewModel<T>(mainDispatcher: CoroutineDispatcher) : ViewModel() {
    private val _content = MutableLiveData<Content<T>>()
    val content: LiveData<Content<T>> = _content

    private val _navigationTarget = MutableLiveData<NavigationTarget>()
    val navigationTarget: LiveData<NavigationTarget> = _navigationTarget

    private val handler = CoroutineExceptionHandler { _, exception -> onFailure(exception) }
    protected val scope = CoroutineScope(SupervisorJob() + handler + mainDispatcher)

    @VisibleForTesting
    internal var loadJob: Job? = null

    private var initialized = false

    init {
        _content.value = Content.Loading
        _navigationTarget.value = NavigationTarget.None
    }

    protected fun onInitialized() {
        if (initialized) {
            throw IllegalStateException("onInitialized called twice")
        }
        initialized = true

        load()
    }

    fun onRetryClicked() {
        if (isFailed()) {
            load()
        }
    }

    fun onNavigated(target: NavigationTarget) {
        val currentTarget = _navigationTarget.value
        if (currentTarget != NavigationTarget.None && currentTarget == target) {
            _navigationTarget.value = NavigationTarget.None
        }
    }

    override fun onCleared() {
        super.onCleared()
        scope.coroutineContext.cancel()
    }

    private fun load() {
        _content.postValue(Content.Loading)
        loadJob?.cancel()
        loadJob = scope.launch {
            loadAsync()
        }
    }

    protected abstract suspend fun loadAsync()

    protected fun navigate(target: NavigationTarget) {
        _navigationTarget.postValue(target)
    }

    protected fun display(content: T) {
        _content.postValue(Content.Loaded(content))
    }

    protected fun isLoaded(): Boolean {
        return _content.value is Content.Loaded && !isPendingNavigation()
    }

    private fun isFailed(): Boolean {
        return _content.value is Content.Error && !isPendingNavigation()
    }

    private fun isPendingNavigation(): Boolean {
        return _navigationTarget.value !is NavigationTarget.None
    }

    private fun onFailure(throwable: Throwable) {
        _content.postValue(Content.Error(throwable.localizedMessage))
    }
}