package com.example.ugurozmen.viewmodel

import com.example.ugurozmen.model.ImageRepository
import com.example.ugurozmen.model.UsageRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named

data class MostViewedCategory(val title: String, val imageUri: String)

class MostViewedCategoryViewModel @Inject constructor(
    private val usageRepository: UsageRepository,
    private val imageRepository: ImageRepository,
    @Named("MainDispatcher") mainDispatcher: CoroutineDispatcher
) : BaseViewModel<MostViewedCategory>(mainDispatcher) {

    private var mostViewedCategory: Category? = null

    init {
        onInitialized()
    }

    fun onNextClicked() {
        if (isLoaded()) {
            mostViewedCategory?.let {
                scope.launch {
                    usageRepository.increaseUsageCount(it.name)
                }
                navigate(NavigationTarget.Images(it.name))
            }
        }
    }

    fun onGoToCategoriesClicked() {
        if (isLoaded()) {
            navigate(NavigationTarget.Categories(true))
        }
    }

    override suspend fun loadAsync() {
        val usages = usageRepository.getMostUsed(1)
        val name = usages.firstOrNull()?.name
        if (name != null) {
            val category = Category.valueOf(name)
            val image = imageRepository.pickRandomImage(category.year)
            val content = MostViewedCategory(category.year.toString(), imageUri = image.url)

            mostViewedCategory = category
            display(content)
        } else {
            navigate(NavigationTarget.Categories(false))
        }
    }
}