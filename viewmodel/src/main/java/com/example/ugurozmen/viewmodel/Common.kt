package com.example.ugurozmen.viewmodel

import org.threeten.bp.Year

enum class Category(val year: Year) {
    picturesOf2013(2013),
    picturesOf2014(2014),
    picturesOf2015(2015),
    picturesOf2016(2016),
    picturesOf2017(2017),
    picturesOf2018(2018);

    constructor(isoYear: Int) : this(Year.of(isoYear))
}