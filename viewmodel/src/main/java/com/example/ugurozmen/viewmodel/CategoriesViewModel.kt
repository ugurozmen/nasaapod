package com.example.ugurozmen.viewmodel

import com.example.ugurozmen.model.ImageRepository
import com.example.ugurozmen.model.Usage
import com.example.ugurozmen.model.UsageRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Named

data class CategoryTile(val imageUri: String, val title: String, val width: Int)

class CategoriesViewModel @Inject constructor(
    private val usageRepository: UsageRepository,
    private val imageRepository: ImageRepository,
    @Named("MainDispatcher") mainDispatcher: CoroutineDispatcher
) : BaseViewModel<List<CategoryTile>>(mainDispatcher) {

    init {
        onInitialized()
    }

    fun onItemSelected(index: Int) {
        if (isLoaded()) {
            val categoryName = Category.values()[index].name
            scope.launch {
                usageRepository.increaseUsageCount(categoryName)
            }
            navigate(NavigationTarget.Images(categoryName))
        }
    }

    override suspend fun loadAsync() {
        val mostUsed = usageRepository.getMostUsed(3)
        val categoryWidths = calculateCategoryWidths(mostUsed)

        val deferredImages = Category.values().map {
            scope.async {
                imageRepository.pickRandomImage(it.year)
            }
        }

        val images = deferredImages.map { it.await() }

        val tiles = Category.values().mapIndexed { i, c ->
            CategoryTile(images[i].url, c.year.toString(), categoryWidths[i])
        }

        display(tiles)
    }

    private fun calculateCategoryWidths(mostUsed: List<Usage>): List<Int> {
        val widthsOfMostUsed = listOf(3, 2)

        return Category.values().map {
            val index = findIndex(mostUsed, it)
            if (index > -1 && index < 2) {
                widthsOfMostUsed[index]
            } else {
                1
            }
        }
    }

    private fun findIndex(mostUsed: List<Usage>, category: Category): Int = mostUsed.indexOfFirst {
        it.name == category.name
    }
}