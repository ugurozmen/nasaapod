package com.example.ugurozmen.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.ugurozmen.model.ImageReference
import com.example.ugurozmen.model.ImageRepository
import com.example.ugurozmen.model.Usage
import com.example.ugurozmen.model.UsageRepository
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.doThrow
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class MostViewCategoryViewModelTest {
    @get:Rule
    var instantExecutorRule: TestRule = InstantTaskExecutorRule()

    @Test
    fun `When initialized Then loading screen is shown`() {
        val usageRepo = mock<UsageRepository> {
            onBlocking { getMostUsed(any()) } doReturn listOf()
        }
        val imageRepo = mock<ImageRepository>()

        val tested = MostViewedCategoryViewModel(usageRepo, imageRepo, Dispatchers.Default)

        Assert.assertEquals(Content.Loading, tested.content.value)
    }

    @Test
    fun `Given no usage data found When initialized Then navigates to Categories`() {
        val usageRepo = mock<UsageRepository> {
            onBlocking { getMostUsed(any()) } doReturn listOf()
        }
        val imageRepo = mock<ImageRepository>()

        val tested = MostViewedCategoryViewModel(usageRepo, imageRepo, Dispatchers.Default)
        tested.waitUntilIdle()

        Assert.assertEquals(Content.Loading, tested.content.value)
        Assert.assertEquals(NavigationTarget.Categories(false), tested.navigationTarget.value)
    }

    @Test
    fun `Given usage data found When initialized Then displays most viewed category`() {
        val mostViewedCategory = Category.picturesOf2017
        val imageUrl = "http://xyz.com/image.jpg"

        val tested = setupCategoryLoaded(mostViewedCategory, imageUrl)
        tested.waitUntilIdle()

        Assert.assertEquals(
            Content.Loaded(MostViewedCategory(mostViewedCategory.year.toString(), imageUrl)),
            tested.content.value
        )
        Assert.assertEquals(NavigationTarget.None, tested.navigationTarget.value)
    }

    @Test
    fun `Given most viewed category displayed When next button clicked Then navigates images of the category`() {
        val mostViewedCategory = Category.picturesOf2017
        val imageUrl = "http://xyz.com/image.jpg"
        val tested = setupCategoryLoaded(mostViewedCategory, imageUrl)

        tested.onNextClicked()

        Assert.assertEquals(NavigationTarget.Images(mostViewedCategory.name), tested.navigationTarget.value)
    }

    @Test
    fun `Given most viewed category displayed When go to categories button clicked Then navigates categories`() {
        val mostViewedCategory = Category.picturesOf2017
        val imageUrl = "http://xyz.com/image.jpg"
        val tested = setupCategoryLoaded(mostViewedCategory, imageUrl)

        tested.onGoToCategoriesClicked()

        Assert.assertEquals(NavigationTarget.Categories(true), tested.navigationTarget.value)
    }


    @Test
    fun `Given image could not be loaded When initialized Then displays the error message`() {
        val mostViewedCategory = Category.picturesOf2017
        val errorMessage = "Server unavailable"

        val tested = setupImageFailed(mostViewedCategory, errorMessage)

        Assert.assertEquals(Content.Error(errorMessage), tested.content.value)
        Assert.assertEquals(NavigationTarget.None, tested.navigationTarget.value)
    }

    @Test
    fun `Given error message is displayed When Retry clicked Then loading screen is shown`() {
        val mostViewedCategory = Category.picturesOf2017
        val errorMessage = "Server unavailable"
        val tested = setupImageFailed(mostViewedCategory, errorMessage)

        tested.onRetryClicked()

        Assert.assertEquals(Content.Loading, tested.content.value)
        Assert.assertEquals(NavigationTarget.None, tested.navigationTarget.value)
    }

    private fun setupCategoryLoaded(
        mostViewedCategory: Category,
        imageUrl: String
    ): MostViewedCategoryViewModel {
        val usageRepo = mock<UsageRepository> {
            onBlocking { getMostUsed(1) } doReturn listOf(
                Usage(mostViewedCategory.name, 10)
            )
        }
        val imageRepo = mock<ImageRepository> {
            onBlocking { pickRandomImage(mostViewedCategory.year) } doReturn ImageReference(imageUrl, "Image")
        }
        val tested = MostViewedCategoryViewModel(usageRepo, imageRepo, Dispatchers.Default)
        tested.waitUntilIdle()
        return tested
    }

    private fun setupImageFailed(
        mostViewedCategory: Category,
        errorMessage: String
    ): MostViewedCategoryViewModel {
        val usageRepo = mock<UsageRepository> {
            onBlocking { getMostUsed(1) } doReturn listOf(
                Usage(mostViewedCategory.name, 10)
            )
        }
        val imageRepo = mock<ImageRepository> {
            onBlocking { pickRandomImage(mostViewedCategory.year) } doThrow RuntimeException(errorMessage)
        }

        val tested = MostViewedCategoryViewModel(usageRepo, imageRepo, Dispatchers.Default)
        tested.waitUntilIdle()
        return tested
    }

    private fun BaseViewModel<*>.waitUntilIdle() {
        runBlocking {
            loadJob?.join()
        }
    }
}