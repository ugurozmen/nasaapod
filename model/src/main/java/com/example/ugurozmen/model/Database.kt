package com.example.ugurozmen.model

import androidx.room.*

@Database(entities = [Usage::class], version = 1, exportSchema = false)
internal abstract class AppDatabase : RoomDatabase() {
    abstract fun usageDao(): UsageDao
}

@Dao
internal abstract class UsageDao {
    @Query("SELECT * FROM Usage ORDER BY count DESC LIMIT :limit")
    abstract fun getUsages(limit: Int): List<Usage>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun createUsage(usage: Usage)

    @Query("UPDATE Usage SET count = count + 1 WHERE name = :name")
    abstract fun increaseCount(name: String): Int

    @Transaction
    open fun createOrIncreaseCount(name: String) {
        val affectedRecords = increaseCount(name)
        if (affectedRecords < 1) {
            createUsage(Usage(name, 1))
        }
    }
}

@Entity
data class Usage(@PrimaryKey val name: String, val count: Long = 0)