package com.example.ugurozmen.model

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.threeten.bp.LocalDate
import org.threeten.bp.Year
import java.util.concurrent.ThreadLocalRandom
import javax.inject.Inject
import javax.inject.Named

class ImageRepository @Inject constructor(private val apodService: ApodService, @Named("apiKey") private val apiKey: String) {
    suspend fun pickRandomImage(year: Year): ImageReference {
        while (true) {
            val day = randomDayOfYear(year)
            val image = getImageOfDay(day)
            if (containsImageUrl(image)) {
                return image
            } else {
                Log.d("ImageRepo", "image url might be containing a video, skipping. $day: ${image.url}")
            }
        }
    }

    private fun randomDayOfYear(year: Year): LocalDate {
        val random = ThreadLocalRandom.current()
        val dayOfYear = random.nextInt(year.length())
        return year.atDay(dayOfYear)
    }

    private suspend fun getImageOfDay(day: LocalDate): ImageReference = withContext(Dispatchers.IO) {
        val date = day.toString()
        apodService.imageOfTheDayAsync(date, apiKey).await()
    }

    private val imageExtension = listOf(".jpg", "jpeg", ".png")
    private val httpSchemas = listOf("http://", "https://")

    private fun containsImageUrl(reference: ImageReference) =
        httpSchemas.any { reference.url.startsWith(it, true) } &&
                imageExtension.any { reference.url.endsWith(it, true) }
}