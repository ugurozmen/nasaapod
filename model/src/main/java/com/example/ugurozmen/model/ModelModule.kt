package com.example.ugurozmen.model

import android.content.Context
import androidx.room.Room
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module(includes = [ServiceModule::class, DbModule::class])
class ModelModule

@Module
class DbModule {
    @Provides
    @Named("dbName")
    fun dbName() = "AppDb"

    @Provides
    @Singleton
    internal fun provideAppDatabase(context: Context, @Named("dbName") dbName: String): AppDatabase =
        Room.databaseBuilder(context, AppDatabase::class.java, dbName).build()

    @Provides
    @Singleton
    internal fun provideUsageDao(appDatabase: AppDatabase): UsageDao = appDatabase.usageDao()
}

@Module
class ServiceModule {
    @Provides
    @Named("baseUrl")
    fun itemId() = "https://api.nasa.gov/"

    @Provides
    @Named("apiKey")
    fun apiKey() = "xJX3cFnFFWFqCdLZxGMVW3RoUwfxW5napck5KYpd"

    @Provides
    @Singleton
    fun provideApodService(retrofit: Retrofit): ApodService = retrofit.create(ApodService::class.java)

    @Provides
    @Singleton
    fun provideRetrofit(@Named("baseUrl") baseUrl: String, client: OkHttpClient, moshi: Moshi): Retrofit =
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient = OkHttpClient.Builder().build()

    @Provides
    @Singleton
    fun provideMoshi(): Moshi = Moshi.Builder().build()
}
