package com.example.ugurozmen.model

import com.squareup.moshi.JsonClass
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface ApodService {
    @GET("planetary/apod")
    fun imageOfTheDayAsync(@Query("date") date: String, @Query("api_key") apiKey: String): Deferred<ImageReference>
}

@JsonClass(generateAdapter = true)
data class ImageReference(val url: String, val title: String)