package com.example.ugurozmen.model

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UsageRepository @Inject internal constructor(private val dao: UsageDao) {
    suspend fun getMostUsed(limit: Int): List<Usage> = withContext(Dispatchers.IO) {
        dao.getUsages(limit)
    }

    suspend fun increaseUsageCount(name: String) {
        withContext(Dispatchers.IO) {
            dao.createOrIncreaseCount(name)
        }
    }
}