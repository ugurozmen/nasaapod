package com.example.ugurozmen.nasaapod

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.ugurozmen.viewmodel.CategoriesViewModel
import com.example.ugurozmen.viewmodel.CategoryTile
import com.example.ugurozmen.viewmodel.Content
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_categories.*
import kotlinx.android.synthetic.main.item_category.view.*
import kotlinx.android.synthetic.main.part_loading_and_error.*
import javax.inject.Inject

class CategoriesFragment : Fragment() {
    @Inject
    lateinit var viewModelProviderFactory: ViewModelProvider.Factory

    private var panels = listOf<View>()

    private val adapter = ItemsAdapter()
    private val spanLookup = SpanLookup(adapter)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_categories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        panels = listOf(loadingPanel, errorPanel, contentPanel)
        (contentPanel.layoutManager as GridLayoutManager).spanSizeLookup = spanLookup
        contentPanel.adapter = adapter
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    private val viewModel: CategoriesViewModel by lazy {
        ViewModelProviders.of(this, viewModelProviderFactory).get(CategoriesViewModel::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter.onClickedCallback = viewModel::onItemSelected

        retryButton.setOnClickListener { viewModel.onRetryClicked() }

        viewModel.content.observe(viewLifecycleOwner, Observer {
            when (it) {
                Content.Loading -> showPanel(loadingPanel)
                is Content.Error -> {
                    val description =
                        if (it.description.isEmpty()) "" else getString(R.string.error_detail, it.description)
                    errorDescription.text = getString(R.string.error_description, description)
                    showPanel(errorPanel)
                }
                is Content.Loaded -> {
                    showPanel(contentPanel)
                    adapter.tiles = it.content
                }
            }
        })

        attachNavigationObserver(this, viewModel)
    }

    override fun onStart() {
        super.onStart()
    }

    private fun showPanel(panel: View) = panels.forEach {
        it.visibility = if (it == panel) View.VISIBLE else View.GONE
    }
}

private class SpanLookup(val adapter: ItemsAdapter) : GridLayoutManager.SpanSizeLookup() {
    override fun getSpanSize(position: Int): Int {
        return adapter.tiles[position].width
    }
}

private class ItemsAdapter : RecyclerView.Adapter<ItemViewHolder>() {
    var onClickedCallback: (Int) -> Unit = {}
    var tiles = listOf<CategoryTile>()
        set(value) {
            if (value != field) {
                field = value
                notifyDataSetChanged()
            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false) as ViewGroup
        return ItemViewHolder(view)
    }

    override fun getItemCount(): Int = tiles.size

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(tiles[position])
        holder.setOnClickedCallback { onClickedCallback(position) }
    }
}

private class ItemViewHolder(val view: ViewGroup) : RecyclerView.ViewHolder(view) {
    fun bind(tile: CategoryTile) {
        view.text.text = tile.title
        Glide.with(view.image).load(tile.imageUri).into(view.image)
    }

    fun setOnClickedCallback(callback: () -> Unit) {
        view.setOnClickListener { callback() }
    }
}