package com.example.ugurozmen.nasaapod

import com.example.ugurozmen.model.ModelModule
import com.example.ugurozmen.viewmodel.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Component(modules = [AndroidInjectionModule::class, ViewModule::class, ViewModelModule::class, ModelModule::class])
@Singleton
interface AppComponent : AndroidInjector<App> {
    @dagger.Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()
}
