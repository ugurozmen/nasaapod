package com.example.ugurozmen.nasaapod

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ViewModule {
    @Binds
    abstract fun bindApplication(app: App): Application

    @Binds
    abstract fun bindContext(application: Application): Context

    @ContributesAndroidInjector
    abstract fun contributeMainActivityInjector(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributeCategoriesActivityInjector(): CategoriesActivity

    @ContributesAndroidInjector
    abstract fun contributeMostViewedCategoryInjector(): MostViewedCategoryFragment

    @ContributesAndroidInjector
    abstract fun contributeCategoriesInjector(): CategoriesFragment

    @ContributesAndroidInjector
    abstract fun contributeImagesInjector(): ImagesFragment
}
