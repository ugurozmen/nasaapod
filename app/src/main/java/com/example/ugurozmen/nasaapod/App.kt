package com.example.ugurozmen.nasaapod

import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class App : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<App> = DaggerAppComponent.builder().create(this)

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
    }
}