package com.example.ugurozmen.nasaapod

import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.ugurozmen.viewmodel.BaseViewModel
import com.example.ugurozmen.viewmodel.NavigationTarget

internal fun attachNavigationObserver(fragment: Fragment, viewModel: BaseViewModel<*>) {
    viewModel.navigationTarget.observe(fragment.viewLifecycleOwner, Observer {
        viewModel.onNavigated(it)
        val targetId = when (it) {
            is NavigationTarget.Categories -> {
                if (!it.addToBackStack) {
                    fragment.activity?.finish()
                    R.id.categoriesActivity
                } else {
                    R.id.categoriesFragment
                }
            }
            is NavigationTarget.Images -> R.id.imagesFragment
            else -> 0
        }
        if (targetId > 0) {
            fragment.findNavController().navigate(targetId)
        }
    })
}