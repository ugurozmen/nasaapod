package com.example.ugurozmen.nasaapod

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.example.ugurozmen.viewmodel.Content
import com.example.ugurozmen.viewmodel.MostViewedCategoryViewModel
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_most_viewed_category.*
import kotlinx.android.synthetic.main.part_loading_and_error.*
import javax.inject.Inject

class MostViewedCategoryFragment : Fragment() {

    @Inject
    lateinit var viewModelProviderFactory: ViewModelProvider.Factory

    private val panels by lazy {
        listOf(loadingPanel, errorPanel, contentPanel)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_most_viewed_category, container, false)
    }

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    private val viewModel: MostViewedCategoryViewModel by lazy {
        ViewModelProviders.of(this, viewModelProviderFactory).get(MostViewedCategoryViewModel::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        retryButton.setOnClickListener { viewModel.onRetryClicked() }
        nextButton.setOnClickListener { viewModel.onNextClicked() }
        categoriesButton.setOnClickListener { viewModel.onGoToCategoriesClicked() }

        viewModel.content.observe(viewLifecycleOwner, Observer {
            when (it) {
                Content.Loading -> showPanel(loadingPanel)
                is Content.Error -> {
                    val description =
                        if (it.description.isEmpty()) "" else getString(R.string.error_detail, it.description)
                    errorDescription.text = getString(R.string.error_description, description)
                    showPanel(errorPanel)
                }
                is Content.Loaded -> {
                    showPanel(contentPanel)
                    description.text = it.content.title
                    Glide.with(this).load(it.content.imageUri).into(image)
                }
            }
        })

        attachNavigationObserver(this, viewModel)
    }

    private fun showPanel(panel: View) = panels.forEach {
        it.visibility = if (it == panel) View.VISIBLE else View.GONE
    }
}